const getSortLabel = (sortOption: string): string => {
  switch (sortOption) {
    case "sponsored":
      return "Featured";
    case "popularity":
      return "Popularity";
    case "fees":
      return "Fees: Low to High";
    case "ratingM":
      return "Value Rating";
    case "ratingP":
      return "Placement Rating";
    case "ratingF":
      return "Faculty Rating";
    default:
      return sortOption;
  }
};

export default getSortLabel;

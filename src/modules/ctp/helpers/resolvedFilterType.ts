import FilterObject from "../types/filterObject";

const resolvedFilterType = (filter: FilterObject) => {
  if (filter.filterType === "city" || filter.filterType === "state")
    return "location";
  else if (
    filter.filterType === "specialization" ||
    filter.filterType === "substream"
  )
    return "specialization";
  else return filter.filterType;
};

export default resolvedFilterType;

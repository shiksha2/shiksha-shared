export default interface FilterObject {
    id: string;
    name: string;
    count: number;
    url: string;
    enabled: boolean;
    checked: boolean;
    filterType: string;
}

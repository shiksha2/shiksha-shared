import FilterObject from "../types/filterObject";
declare const resolvedFilterType: (filter: FilterObject) => string;
export default resolvedFilterType;

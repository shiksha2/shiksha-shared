
# shiksha-shared

This package contains shared utilities for the Shiksha projects.


## Installation

- To install shiksha-shared, add following dependency in package.json

```json
  "dependencies": {
    "shiksha-shared": "git+https://sscodmobile:Qwerty@12345@gitlab.com/shiksha2/shiksha-shared.git#main"
  }
```
- Run ``` npm install ``` or ```yarn``` to install the added dependency

  
## Usage/Examples

```javascript
import {fun1, fun2} from 'shiksha-shared/dist/modules/ctp'

console.log(fun1())
```

## Important
After contributing to shiksha-shared, don't forget to run ```npm run build``` or ```yarn build``` before pushing to remote repository.

## Folder Structure
```
shiksha-shared
├── dist  (transpiled .js and .d.ts files)
├── src
│   ├── modules
│   │   ├── moduleA
│   │   │   ├── ...
│   │   │   ├── ...
│   │   │   └── index.ts
│   │   ├── moduleB
│   │   │   ├── ...
│   │   │   ├── ...
│   │   │   └── index.ts
│   │   ├── ...
│   │   └── ...
│   └── index.ts
├── .gitignore
├── package.json
└── tsconfig.json
```
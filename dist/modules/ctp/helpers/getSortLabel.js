"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getSortLabel = (sortOption) => {
    switch (sortOption) {
        case "sponsored":
            return "Featured";
        case "popularity":
            return "Popularity";
        case "fees":
            return "Fees: Low to High";
        case "ratingM":
            return "Value Rating";
        case "ratingP":
            return "Placement Rating";
        case "ratingF":
            return "Faculty Rating";
        default:
            return sortOption;
    }
};
exports.default = getSortLabel;

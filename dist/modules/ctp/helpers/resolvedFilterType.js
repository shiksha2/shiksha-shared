"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const resolvedFilterType = (filter) => {
    if (filter.filterType === "city" || filter.filterType === "state")
        return "location";
    else if (filter.filterType === "specialization" ||
        filter.filterType === "substream")
        return "specialization";
    else
        return filter.filterType;
};
exports.default = resolvedFilterType;
